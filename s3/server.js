const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

let products = [
    {
        id: 0,
        productName: 'Samsung Galaxy S9',
        price: 4000
    },
    {
        id: 1,
        productName: 'Iphone XR',
        price: 3999
    }
];

app.get('/get-all', (req, res) => {
    res.status(200).send(products);
})

app.post('/add', (req, res) => {
    if(req.body.productName && req.body.price){
        let product = {
            id: products.length,
            productName: req.body.productName,
            price: req.body.price
        };
        products.push(product);
        res.status(200).send(product);
    } else {
        res.status(500).send('Error!');
    }
});

app.put('/update/:id', (req, res) =>{
    const id = req.params.id;
    let updated = false;
    products.forEach((product) =>{
       if(product.id == id){
           updated = true;
           product.productName = req.body.productName;
           product.price = req.body.price;
           
       } 
    });
    if(updated){
     res.status(200).send(`Product ${id} updated!`);    
    } else {
        res.status(404).send(`Could not find resource with id ${id}`);
    }
    
 });

 app.delete("/delete", (req, res)=>{
    var deleted=false;
    var Name=req.body.productName;
    products.forEach((product) =>{
        if(product.productName.localeCompare(Name))
        {
            products.pop(product);
            deleted=true;
        }
    
    });
    if(deleted){
        res.status(200).send(`Item deleted!!`);    
       } else {
           res.status(404).send(`Erorr`);
       }

});
    


app.listen(8080, () => {
    console.log('Server started on port 8080...');
});