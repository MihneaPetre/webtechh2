import React, {Component} from 'react';
import {Add} from './components/add/Add';
import {GetAll} from './components/getall/GetAll';



class App extends Component {

  constructor(props){
    super(props);
    this.state = {};
    this.state.products = [];
  }

  onProductAdded = (product) => {
    let products=this.state.products;
    products.push(product);
    this.setState({
      products: products
    })
     console.log(this.state.products)
  }

  componentWillMount(){
    fetch('https://myworkspace-mihneaptr.c9users.io/get-all')
    .then((res) => 
        res.json())
    .then((products) =>{
      this.setState({
        products: products
      })
    })
  }
  render() {
    return(
      <React.Fragment>
          <Add productAdded={this.onProductAdded} />
          <GetAll source={this.state.products}/>
    
      </React.Fragment>

    )
  }
}

export default App;
