import React from 'react';
import axios from 'axios';


export class Add extends React.Component{

    constructor(props)
    {
        super(props);
        this.state={};
        this.state.id=0;
        this.state.productName="";
        this.state.price=0;

    }

    handleChangeProductName=(event)=>{
        this.setState({
            productName: event.target.value
        })
    }
    handleChangePrice=(event)=>{
        this.setState({
            price: event.target.value
        })
    }
    handleAddClick=()=>{
       let product={
        productName: this.state.productName,
        price: this.state.price
       }
    
       axios.post('https://myworkspace-mihneaptr.c9users.io/add', product).then((res)=>{
           if(res.status===200)
           {
               this.props.productAdded(product)
           }
       }).catch((err)=>{
                console.log(err)
       })
 
    }
    render(){
        return (
            <div>
                <h1>Add a product</h1>
                <input type="text" placeholder="Product Name" 
                onChange={this.handleChangeProductName} 
                value={this.state.productName}/>
                 <input type="number" placeholder="Product Price" 
                onChange={this.handleChangePrice} 
                value={this.state.price}/>
                <button onClick={this.handleAddClick}>Add product</button>

            </div>

        )

    }
}